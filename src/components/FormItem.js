import React, { Component } from 'react';
import { Card, Form, Input, Button, Select, Row, Col, DatePicker, Radio, InputNumber } from 'antd';
import { connect } from 'react-redux';
import { updateItem, resetItem } from '../redux/actions/ItemActions';
import { isSubmitActive, resrtStatus } from '../redux/actions/StatusActions';
import { addListItem, editDataListItem } from '../redux/actions/ListItemAction';
import moment from 'moment';
import PhoneInput from 'react-phone-input-2';
import NatianalityMock from '../mock/NatianalityMock';
import TitleMock from '../mock/TitleMock';
import GenderMock from '../mock/GenderMock';
import CountriesMock from '../mock/CountriesMock';


export class FormItem extends Component {
    constructor(){
        super()
        this.citizenIdDetail2 = React.createRef();
        this.citizenIdDetail3 = React.createRef();
        this.citizenIdDetail4 = React.createRef();
        this.citizenIdDetail5 = React.createRef();
    }

    fnSubmit = () => {
        this.props.isSubmitActive(this.props.status);
        if (this.fnValidate("title") || this.fnValidate("name") ||
            this.fnValidate("lastname") || this.fnValidate("birthday") ||
            this.fnValidate("mobilePhoneNumber") || this.fnValidate("expectedSalary")) {
            return;
        }
        if (this.props.status.isEdit) {
            this.props.editDataListItem(this.props.listItem, this.props.item, this.props.status.indexEdit)
        } else {
            this.props.addListItem(this.props.listItem, this.props.item);
        }

        this.props.resrtStatus();
        this.props.resetItem();
    }

    fnValidate = (type) => {
        let result = false;
        switch (type) {
            case "title": result = (this.props.item.title === null || this.props.item.title === ""); break;
            case "name": result = (this.props.item.name === null || this.props.item.name === ""); break;
            case "lastname": result = (this.props.item.lastname === null || this.props.item.lastname === ""); break;
            case "birthday": result = (this.props.item.birthday === null || this.props.item.birthday === ""); break;
            case "mobilePhoneNumber": result = (this.props.item.mobilePhoneNumber === null || this.props.item.mobilePhoneNumber === ""); break;
            case "expectedSalary": result = (this.props.item.expectedSalary === null || this.props.item.expectedSalary === ""); break;
            default: break;
        }
        return result;
    }

    render = () => {
        const { updateItem, item, status } = this.props;
        return (
            <Card style={{ boxShadow: '0 3px 6px 0 rgba(0,0,0,0.2)', marginBottom: '25px' }}>
                <Form >
                    <Row justify="space-between">
                        <Col xs={24} sm={24} md={5} >
                            <Form.Item
                                label="Title" required
                                validateStatus={status.isSubmit && this.fnValidate("title") ? "error" : "success"}
                            >
                                <Select
                                    value={item.title}
                                    onSelect={(data) => { updateItem(item, "title", data) }}
                                >
                                    {TitleMock.map((data, index) =>
                                        (<Select.Option key={index} value={data} >{data}</Select.Option>)
                                    )}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={9}>
                            <Form.Item
                                label="Name" required
                                validateStatus={status.isSubmit && this.fnValidate("name") ? "error" : "success"}
                            >
                                <Input
                                    value={item.name}
                                    onChange={(data) => updateItem(item, "name", data.target.value)}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={9}>
                            <Form.Item
                                label="Lastname" required
                                validateStatus={status.isSubmit && this.fnValidate("lastname") ? "error" : "success"}
                            >
                                <Input
                                    value={item.lastname}
                                    onChange={(data) => updateItem(item, "lastname", data.target.value)}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row justify="space-between">
                        <Col xs={24} sm={24} md={9}>
                            <Form.Item
                                label="Birthday" required
                                validateStatus={status.isSubmit && this.fnValidate("birthday") ? "error" : "success"}
                            >
                                <DatePicker
                                    format="MM/DD/YY"
                                    style={{ width: '100%' }}
                                    value={item.birthday == null ? null : moment(item.birthday, "MM/DD/YY")}
                                    onChange={(date, dateString) => { updateItem(item, "birthday", dateString) }}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={13}>
                            <Form.Item label="Nationality">
                                <Select
                                    placeholder="-- Please Select --"
                                    value={item.nationality}
                                    onSelect={(data) => { updateItem(item, "nationality", data) }}
                                >
                                    {NatianalityMock.map((data, index) => (
                                        <Select.Option key={index} value={data}>{data}</Select.Option>)
                                    )}
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row >
                        <Col xs={24} sm={24} md={2} style={{ paddingRight: '5px' }}>
                            <Form.Item label="Citizen ID">
                                <Input
                                    maxLength={1}
                                    style={{ textAlign: 'center' }}
                                    value={item.citizenIdDetail1}
                                    onChange={(data) => { 
                                        updateItem(item, "citizenIdDetail1", data.target.value);
                                        if(data.target.value.length === 1) this.citizenIdDetail2.current.focus(); 
                                    }}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={3} style={{ paddingRight: '5px' }}>
                            <Form.Item label="-" colon={false}>
                                <Input
                                    maxLength={4}
                                    style={{ textAlign: 'center' }}
                                    value={item.citizenIdDetail2}
                                    onChange={(data) => { 
                                        updateItem(item, "citizenIdDetail2", data.target.value);
                                        if(data.target.value.length === 4) this.citizenIdDetail3.current.focus();  
                                    }}
                                    ref={this.citizenIdDetail2}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={3} style={{ paddingRight: '5px' }}>
                            <Form.Item label="-" colon={false}>
                                <Input
                                    maxLength={4}
                                    style={{ textAlign: 'center' }}
                                    value={item.citizenIdDetail3}
                                    onChange={(data) => { 
                                        updateItem(item, "citizenIdDetail3", data.target.value);
                                        if(data.target.value.length === 4) this.citizenIdDetail4.current.focus();  
                                    }}
                                    ref={this.citizenIdDetail3}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={3} style={{ paddingRight: '5px' }}>
                            <Form.Item label="-" colon={false}>
                                <Input
                                    maxLength={3}
                                    style={{ textAlign: 'center' }}
                                    value={item.citizenIdDetail4}
                                    onChange={(data) => { 
                                        updateItem(item, "citizenIdDetail4", data.target.value);
                                        if(data.target.value.length === 3) this.citizenIdDetail5.current.focus();
                                    }}
                                    ref={this.citizenIdDetail4}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={1} style={{ paddingRight: '5px' }}>
                            <Form.Item label="-" colon={false}>
                                <Input
                                    maxLength={1}
                                    style={{ textAlign: 'center' }}
                                    value={item.citizenIdDetail5}
                                    onChange={(data) => { updateItem(item, "citizenIdDetail5", data.target.value) }}
                                    ref={this.citizenIdDetail5}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={24} sm={24} md={12}>
                            <Form.Item label="Gender" >
                                <Radio.Group
                                    value={item.gender}
                                    onChange={(data) => { updateItem(item, "gender", data.target.value) }}
                                >
                                    {GenderMock.map((data, index) =>
                                        (<Radio key={index} value={data}>{data}</Radio>)
                                    )}
                                </Radio.Group>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} sm={12} md={4} style={{ paddingRight: '5px' }}>
                            <Form.Item label="Mobile Phone" required>
                                <PhoneInput
                                    inputStyle={{ width: '100%', height: '32px' }}
                                    value={item.mobilePhoneCountry}
                                    onlyCountries={CountriesMock}
                                    onChange={(data) => { updateItem(item, "mobilePhoneCountry", data) }}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={12} sm={12} md={5} style={{ paddingRight: '5px' }}>
                            <Form.Item
                                label=" - " colon={false}
                                validateStatus={status.isSubmit && this.fnValidate("mobilePhoneNumber") ? "error" : "success"}
                            >
                                <Input
                                    value={item.mobilePhoneNumber}
                                    onChange={(data) => updateItem(item, "mobilePhoneNumber", data.target.value)}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={24} sm={24} md={8}>
                            <Form.Item label="Passport No"  >
                                <Input
                                    value={item.passportNo}
                                    onChange={(data) => updateItem(item, "passportNo", data.target.value)}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={24} sm={24} md={8} style={{ paddingRight: '5px' }}>
                            <Form.Item
                                label="Expected Salary" required
                                validateStatus={status.isSubmit && this.fnValidate("expectedSalary") ? "error" : "success"}
                            >
                                <InputNumber
                                    style={{ width: '100%' }}
                                    value={item.expectedSalary}
                                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                    onChange={(data) => { updateItem(item, "expectedSalary", data) }}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={1} sm={1} md={1} style={{ textAlign: 'right' }}>
                            <label>THB</label>
                        </Col>
                        <Col xs={24} sm={24} md={3} push={13} >
                            <Button onClick={this.fnSubmit}>SUBMIT</Button>
                        </Col>
                    </Row>
                </Form>
            </Card>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        item: state.item,
        status: state.status,
        listItem: state.listItem
    };
}

export default connect(mapStateToProps, { updateItem, isSubmitActive, resrtStatus, resetItem, addListItem, editDataListItem })(FormItem); 
