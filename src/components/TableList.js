import React, { Component } from 'react';
import { Card, Table, Checkbox, Row, Col, Button } from 'antd';
import { connect } from 'react-redux';
import { setupListItem, deleteListItem } from '../redux/actions/ListItemAction';
import { setupItem } from '../redux/actions/ItemActions';
import { isSubmitEdit } from '../redux/actions/StatusActions';

export class TableList extends Component {

    state = {
        rowSelect: [],
        isSelectAll: false
    }

    componentDidMount = () => {
        let listItem = localStorage.getItem("listItem");
        if (listItem == null) {
            let temp = [];
            this.props.setupListItem(temp);
        } else {
            this.props.setupListItem(JSON.parse(listItem));
        }
    }

    fnIsSelect = (index) => {
        let inSelect = this.state.rowSelect.filter(i => i === index);
        return inSelect.length > 0;
    }

    fnSelect = (index, checked) => {
        let newRowSelect = [];
        let isSelectAll = this.state.isSelectAll;
        if (checked) {
            newRowSelect = this.state.rowSelect;
            newRowSelect.push(index);
        } else {
            newRowSelect = this.state.rowSelect.filter(i => i !== index);
            isSelectAll = false;
        }
        this.setState({ rowSelect: newRowSelect, isSelectAll: isSelectAll });
    }

    fnSelectAll = (checked) => {
        let newRowSelect;
        let isSelectAll = this.state.isSelectAll;
        if (checked) {
            newRowSelect = this.props.listItem.map((data, index) => index);
            if (newRowSelect.length === 0) {
                isSelectAll = false;
            }else{
                isSelectAll = true;
            }
        } else {
            newRowSelect = [];
            isSelectAll = false;
        }
        this.setState({ rowSelect: newRowSelect, isSelectAll: isSelectAll });
    }

    render = () => {
        const { listItem, deleteListItem, setupItem, status, isSubmitEdit } = this.props;
        const columns = [
            {
                key: '#',
                align: 'center',
                width: 50,
                title: '',
                render: (text, record, index) =>
                    (<React.Fragment>
                        <Checkbox
                            checked={this.fnIsSelect(index)}
                            onChange={(i) => { this.fnSelect(index, i.target.checked); }}
                        />
                    </React.Fragment>)
            },
            {
                key: 'name',
                title: 'NAME',
                dataIndex: 'fullName',
            },
            {
                key: 'name',
                title: 'GENDER',
                dataIndex: 'gender',
            },
            {
                key: 'mobilePhone',
                title: 'MOBILE PHONE',
                dataIndex: 'mobilePhone',
            },
            {
                key: 'natianality',
                title: 'NATIANALITY',
                dataIndex: 'nationality',
            },
            {
                key: '#',
                align: 'center',
                width: 250,
                title: '',
                render: (text, record, index) =>
                    (<React.Fragment>
                        <Row>
                            <Col xs={24} sm={24} md={11}>
                                <Button
                                    style={{ width: '100%' }}
                                    onClick={() => {
                                        setupItem(record);
                                        isSubmitEdit(status, index);
                                    }}
                                >
                                    EDIT
                                </Button>
                            </Col>
                            <Col xs={24} sm={24} md={11} push={1}>
                                <Button
                                    onClick={() => {
                                        deleteListItem(listItem, [index]);
                                        this.setState({ rowSelect: [], isSelectAll: false });
                                    }}
                                    style={{ width: '100%' }}
                                >
                                    DELETE
                                </Button>
                            </Col>
                        </Row>
                    </React.Fragment>)
            },
        ];

        return (
            <Card style={{ boxShadow: '0 3px 6px 0 rgba(0,0,0,0.2)', marginBottom: '25px' }}>
                <Row>
                    <Col xs={12} sm={12} md={2}>
                        <Checkbox
                            checked={this.state.isSelectAll}
                            onChange={(c) => { this.fnSelectAll(c.target.checked); }}>
                            Select All
                        </Checkbox>
                    </Col>
                    <Col xs={12} sm={12} md={2}>
                        <Button onClick={() => {
                            deleteListItem(listItem, this.state.rowSelect);
                            this.setState({ rowSelect: [], isSelectAll: false });
                        }}>
                            DELETE
                        </Button>
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col sm={24} md={24}>
                        <Table
                            dataSource={listItem}
                            size='small'
                            columns={columns}
                            rowKey={(record, index) => index}
                            pagination={{ defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['5', '10', '20', '30'] }}
                        />
                    </Col>
                </Row>

            </Card>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        item: state.item,
        status: state.status,
        listItem: state.listItem
    };
}

export default connect(mapStateToProps, { setupListItem, deleteListItem, setupItem, isSubmitEdit })(TableList); 
