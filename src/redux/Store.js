import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import * as Item from './reducers/Item';
import * as ListItem from './reducers/ListItem';
import * as Status from './reducers/Status';

const rootReducer = combineReducers({
    item: Item.reducer,
    listItem: ListItem.reducer,
    status: Status.reducer
});

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

export default store;
