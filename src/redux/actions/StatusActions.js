export const isSubmitActive = (data) => {
    let newState = {...data};
    newState.isSubmit = true;
    return dispatch => { dispatch({ type: "UPDATE_Status", payload: {...newState} }) };
}

export const isSubmitEdit = (data,index) => {
    let newState = {...data};
    newState.isSubmit = false;
    newState.isEdit = true;
    newState.indexEdit = index;
    return dispatch => { dispatch({ type: "UPDATE_Status", payload: {...newState} }) };
}

export const resrtStatus = () => {
    return dispatch => { dispatch({ type: "RESET_Status", payload: {} }) };
}