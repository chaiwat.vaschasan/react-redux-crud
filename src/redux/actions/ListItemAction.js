export const addListItem = (state, data) => {
    let newState = [...state];
    let newData = {
        ...data,
        fullName: data.title + " " + data.name + " " + data.lastname,
        mobilePhone: data.mobilePhoneCountry + data.mobilePhoneNumber,
        citizenId: data.citizenIdDetail1 + data.citizenIdDetail2 + data.citizenIdDetail3 + data.citizenIdDetail4 + data.citizenIdDetail5
    };
    newState.push(newData);
    localStorage.setItem("listItem", JSON.stringify(newState));
    return dispatch => { dispatch({ type: "UPDATE_ListItem", payload: newState }) };
}

export const setupListItem = (state) => {
    let newState = [...state];
    return dispatch => { dispatch({ type: "UPDATE_ListItem", payload: newState }) };
}

export const editDataListItem = (state, data, rowSelect) => {
    let newState = state.map((item, index) => {
        if (index === rowSelect) {
            data.fullName = data.title + " " + data.name + " " + data.lastname;
            data.mobilePhone = data.mobilePhoneCountry + data.mobilePhoneNumber;
            data.citizenId = data.citizenIdDetail1 + data.citizenIdDetail2 + data.citizenIdDetail3 + data.citizenIdDetail4 + data.citizenIdDetail5;
            return data
        } else { return item; }
    });
    console.log(newState);
    localStorage.setItem("listItem", JSON.stringify(newState));
    return dispatch => { dispatch({ type: "UPDATE_ListItem", payload: [...newState] }) };
}

export const deleteListItem = (state, rowSelectList) => {
    let newState = [...state];
    let select = [...rowSelectList];
    newState = newState.filter((data, index) => !select.includes(index));
    console.log(select);
    console.log(newState);
    localStorage.setItem("listItem", JSON.stringify(newState));
    return dispatch => { dispatch({ type: "UPDATE_ListItem", payload: newState }) };
}
