

export const updateItem = (state,type,data) => {
    let newState = {...state};
    switch (type) {
        case "title": newState.title = data; break;
        case "name": newState.name = data; break;
        case "lastname": newState.lastname = data; break;
        case "birthday": newState.birthday = data; break;
        case "nationality": newState.nationality = data; break;
        case "citizenIdDetail1": newState.citizenIdDetail1 = data; break;
        case "citizenIdDetail2": newState.citizenIdDetail2 = data; break;
        case "citizenIdDetail3": newState.citizenIdDetail3 = data; break;
        case "citizenIdDetail4": newState.citizenIdDetail4 = data; break;
        case "citizenIdDetail5": newState.citizenIdDetail5 = data; break;
        case "gender": newState.gender = data; break;
        case "mobilePhoneCountry": newState.mobilePhoneCountry ="+" + data; break;
        case "mobilePhoneNumber": newState.mobilePhoneNumber = data; break;
        case "passportNo": newState.passportNo = data; break;
        case "expectedSalary": newState.expectedSalary = data; break;
        default: break;
    }
    return dispatch => { dispatch({ type: "UPDATE_Item", payload: {...newState} }) };
}

export const setupItem = (state) => {
    return dispatch => { dispatch({ type: "UPDATE_Item", payload: {...state} }) };
}

export const resetItem = () => {
    return dispatch => { dispatch({ type: "RESET_Item", payload: {} }) };
}