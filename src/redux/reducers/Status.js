const initialState = {
    isSubmit: false,
    isEdit: false,
    indexEdit: null
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "UPDATE_Status": state = action.payload; break;
        case "RESET_Status": state = initialState; break;
        default: break;
    }
    return state;

}