const initialState = {
    title:null,
    name:null,
    lastname:null,
    birthday:null,
    nationality:null,
    citizenIdDetail1:"",
    citizenIdDetail2:"",
    citizenIdDetail3:"",
    citizenIdDetail4:"",
    citizenIdDetail5:"",
    gender:null,
    mobilePhoneCountry:"+66",
    mobilePhoneNumber:null,
    passportNo:null,
    expectedSalary:null
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "UPDATE_Item" : state = action.payload; break;
        case "RESET_Item" : state = initialState; break;
        default: break;
    }
    return state;
}