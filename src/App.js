import React from 'react';
import {Layout} from 'antd';
import FormItem from './components/FormItem';
import TableList from './components/TableList';
const {Content} = Layout;

function App() {
  return (
    <div >
      <Content style={{ padding: '0 50px', marginTop: 64 }}>
        <FormItem />
        <TableList />
      </Content>
    </div>
  );
}

export default App;
